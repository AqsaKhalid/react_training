import React, {Component} from 'react';
import './App.css';
import LoginForm from "./components/loginForm";
import Dashboard from "./components/dashboard";

class App extends Component {
    state = {
        isLoggedIn: false,
    };

    handleLogin = () => {
        this.setState({isLoggedIn: !this.state.isLoggedIn});
        return false;
    };

    render() {
        return (
            <div className="container">
                <div className="row mt-lg-5">
                    <div className="col-md-2 col-lg-3"/>
                    {this.state.isLoggedIn ? <Dashboard/> : <LoginForm onLogin={this.handleLogin} />}
                    <div className="col-md-2 col-lg-3"/>
                </div>
            </div>
        )
    };
}

export default App;
