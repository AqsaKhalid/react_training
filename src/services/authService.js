import {apiUrl} from "../config.json";
import axios from "axios";

const apiEndpoint = apiUrl + "login/";
const qs = require('querystring');

export async function login(username, password) {
    return await axios.post(apiEndpoint, qs.stringify({username, password}), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
}

export default {
    login
};
