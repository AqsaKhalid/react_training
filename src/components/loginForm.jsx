import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import auth from "../services/authService";

class LoginForm extends Form {
    state = {
        data: {username: "", password: ""},
        errors: {}
    };

    schema = {
        username: Joi.string()
            .required()
            .label("Username"),
        password: Joi.string()
            .required()
            .label("Password")
    };

    doSubmit = async () => {
        // Call the server
        let resp = await auth.login(this.state.data['username'], this.state.data['password']);
        if (resp.data['status'] === 401) {
            let error = {password: "Incorrect Username or Password"};
            this.setState({errors: error})
        } else {
            this.props.onLogin();
        }
    };

    render() {
        return (
            <div className="col-lg-6 col-md-8">
                <form onSubmit={this.handleSubmit}>
                    <h1 className="text-center">Login</h1>
                    {this.renderInput("username", "Username")}
                    {this.renderInput("password", "Password", "password")}
                    {this.renderButton("Login")}
                </form>
            </div>
        );
    }
}

export default LoginForm;
